module carnaval/scraper

go 1.13

require (
	cloud.google.com/go/datastore v1.0.0
	github.com/gocolly/colly/v2 v2.0.1
	github.com/google/uuid v1.1.1 // indirect
	github.com/kr/pretty v0.2.0
	github.com/sergi/go-diff v1.1.0 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	googlemaps.github.io/maps v0.0.0-20200130222743-aef6b08443c7
)
