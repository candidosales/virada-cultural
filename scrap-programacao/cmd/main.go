package main

import (
	"carnaval/scraper/gmaps"
	"fmt"
	"strings"
	"time"
)

const (
	// patternTime - Format time
	patternDateTime = "2006-01-02T15:04:05"

	mapsApiKey = "AIzaSyC0M0JKC_gVqVbhZ-yBGf_SSgztlOyoZ0M"
)

func main() {

	gmapsClient, _ := gmaps.NewService(mapsApiKey)

	resp, err := gmapsClient.TextSearch("Rua Marselhesa , São Paulo - SP")
	if err != nil {

	}

	fmt.Printf("resp: %#v", resp)
	// mapsClient.
	// c := colly.NewCollector(
	// 	colly.CacheDir("./cache/"),
	// )

	// events := []Event{}
	// eventTypes := []EventType{}
	// places := []Place{}

	// c.OnRequest(func(r *colly.Request) {
	// 	fmt.Println("Visiting", r.URL)
	// })

	// c.OnHTML(".local-evento > ul.lista-tipos", func(e *colly.HTMLElement) {
	// 	e.ForEach("li.clicktipo", func(_ int, e *colly.HTMLElement) {
	// 		place := Place{}

	// 		title := e.Attr("title")
	// 		handleLocation(&place, title)
	// 		places = append(places, place)
	// 	})

	// 	for i, place := range places {
	// 		fmt.Printf("Place %d: %#v \n", i, place)
	// 	}
	// })

	// c.OnHTML(".estilo-evento > ul.lista-tipos", func(e *colly.HTMLElement) {
	// 	e.ForEach("li.clicktipo", func(_ int, e *colly.HTMLElement) {
	// 		eventType := EventType{}
	// 		eventType.Name = e.Attr("title")

	// 		eventTypes = append(eventTypes, eventType)
	// 	})

	// 	for i, eventType := range eventTypes {
	// 		fmt.Printf("Type %d: %#v \n", i, eventType)
	// 	}
	// })

	// c.OnHTML("div.blocos.w-100.mx-auto.ajax-recebedor", func(e *colly.HTMLElement) {
	// 	e.ForEach("div.bloco:not(.d-flex)", func(_ int, e *colly.HTMLElement) {
	// 		event := Event{}
	// 		event.Name = e.ChildText("div.besquerda > p.blocotit")
	// 		event.Description = e.ChildText("p.blocoitine")

	// 		_, err := handleDateTime(&event, e.ChildText(".bdireita > p.pdata"))
	// 		if err != nil {
	// 			fmt.Printf("err: %#v \n", err)
	// 		}

	// 		events = append(events, event)
	// 	})

	// 	for i, event := range events {
	// 		fmt.Printf("Event %d: %#v \n", i, event)
	// 	}

	// })

	// c.Visit("http://localhost:8000/html/page.html")
}

func handleDateTime(e *Event, dateTime string) (string, error) {
	dateTimeArray := strings.Split(dateTime, " | ")
	dateArray := strings.Split(dateTimeArray[0], "/")
	dateReverse := reverse(dateArray)

	date := strings.Join(dateReverse, "-")
	hour := strings.TrimSpace(dateTimeArray[1]) + ":00"

	startedAt, err := time.Parse(patternDateTime, date+"T"+hour)
	if err != nil {
		return "", err
	}

	e.StartedAt = startedAt.Format("2006-01-02 15:04:05")

	return "", nil
}

func reverse(numbers []string) []string {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
	return numbers
}

func handleLocation(p *Place, title string) {
	p.Name = title
	p.Neighborhood = title

	placeNames := strings.Split(title, "/")

	if len(placeNames) > 1 && placeNames[1] != "" {
		p.Neighborhood = strings.TrimSpace(placeNames[1])
	}
}
