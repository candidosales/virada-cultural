package main

type Event struct {
	Name        string
	Description string
	StartedAt   string
	Types       []EventType
	Place       Place
}

type EventType struct {
	Name  string
	Color string
}

type Place struct {
	Lat          float64
	Lng          float64
	Name         string
	Neighborhood string
}
