package gmaps

import (
	"net/url"
	"reflect"
	"regexp"
	"testing"
)

func TestRegexAndUnescape(t *testing.T) {

	r := regexp.MustCompile(regexQuery)

	tests := map[string]struct {
		url          string
		wantResult   string
		wantUnescape string
	}{
		"1": {
			url:          "https://www.google.com/maps/search/Rua Marselhesa+,+São+Paulo+-+SP",
			wantResult:   "Rua Marselhesa+,+São+Paulo+-+SP",
			wantUnescape: "Rua Marselhesa , São Paulo - SP",
		},
		"2": {
			url:          "https://www.google.com/maps/search/Av. Valdemar Ferreira, 53+,+São+Paulo+-+SP",
			wantResult:   "Av. Valdemar Ferreira, 53+,+São+Paulo+-+SP",
			wantUnescape: "Av. Valdemar Ferreira, 53 , São Paulo - SP",
		},
		"3": {
			url:          "https://www.google.com/maps/search/Escadaria do Bixiga - Rua 13 de Maio+,+São+Paulo+-+SP",
			wantResult:   "Escadaria do Bixiga - Rua 13 de Maio+,+São+Paulo+-+SP",
			wantUnescape: "Escadaria do Bixiga - Rua 13 de Maio , São Paulo - SP",
		},
		"4": {
			url:          "https://www.google.com/maps/search/Praça Irmãos Karmann+,+São+Paulo+-+SP",
			wantResult:   "Praça Irmãos Karmann+,+São+Paulo+-+SP",
			wantUnescape: "Praça Irmãos Karmann , São Paulo - SP",
		},
		"5": {
			url:          "https://www.google.com/maps/search/Rua Diana, 90+,+São+Paulo+-+SP",
			wantResult:   "Rua Diana, 90+,+São+Paulo+-+SP",
			wantUnescape: "Rua Diana, 90 , São Paulo - SP",
		},
		"6": {
			url:          "https://www.google.com/maps/search/Praça Haruo Uoya, S/N+,+São+Paulo+-+SP",
			wantResult:   "Praça Haruo Uoya, S/N+,+São+Paulo+-+SP",
			wantUnescape: "Praça Haruo Uoya, S/N , São Paulo - SP",
		},
		"7": {
			url:          "https://www.google.com/maps/search/Avenida Brigadeiro Faria Lima, 4150+,+São+Paulo+-+SP",
			wantResult:   "Avenida Brigadeiro Faria Lima, 4150+,+São+Paulo+-+SP",
			wantUnescape: "Avenida Brigadeiro Faria Lima, 4150 , São Paulo - SP",
		},
		"8": {
			url:          "hhttps://www.google.com/maps/search/Rua Belmiro Braga+,+São+Paulo+-+SP",
			wantResult:   "Rua Belmiro Braga+,+São+Paulo+-+SP",
			wantUnescape: "Rua Belmiro Braga , São Paulo - SP",
		},
		"9": {
			url:          "https://www.google.com/maps/search/Praça Rio dos Campos+,+São+Paulo+-+SP",
			wantResult:   "Praça Rio dos Campos+,+São+Paulo+-+SP",
			wantUnescape: "Praça Rio dos Campos , São Paulo - SP",
		},
		"10": {
			url:          "https://www.google.com/maps/search/Avenida Pedro Álvares Cabral, 220+,+São+Paulo+-+SP",
			wantResult:   "Avenida Pedro Álvares Cabral, 220+,+São+Paulo+-+SP",
			wantUnescape: "Avenida Pedro Álvares Cabral, 220 , São Paulo - SP",
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			// t.Parallel()
			results := r.FindAllSubmatch([]byte(tt.url), -1)
			resultString := string(results[0][1])
			resultUnescape, _ := url.QueryUnescape(resultString)

			if !reflect.DeepEqual(resultString, tt.wantResult) {
				t.Errorf("want resultString to contain %#v and receive %#v ", tt.wantResult, resultString)
			}

			if !reflect.DeepEqual(resultUnescape, tt.wantUnescape) {
				t.Errorf("want resultUnescape to contain %#v and receive %#v ", tt.wantUnescape, resultUnescape)
			}
		})
	}
}
