package gmaps

import (
	"context"
	"log"
	"net/url"
	"regexp"

	"googlemaps.github.io/maps"
)

const (
	// patternTime - Format time
	regexQuery = `https://www\.google\.com/maps/search/([\D\d]+)`
)

// Service -
type Service struct {
	gmaps *maps.Client
}

func NewService(apiKey string) (*Service, error) {
	mapsClient, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		return nil, err
	}

	return &Service{
		gmaps: mapsClient,
	}, nil
	// if err != nil {
	// 	log.Fatalf("fatal error: %s", err)
	// }

	// https://www\.google\.com/maps/search/([\D]+)

	// r := &maps.TextSearchRequest{
	// 	Query: "Largo Ubirajara , São Paulo",
	// }

	// resp, err := c.TextSearch(context.Background(), r)
	// check(err)

	// fmt.Printf("resp: %#v", resp.Results[0].Geometry.Location)
	// pretty.Println(resp)
}

func (s *Service) TextSearch(query string) (maps.PlacesSearchResponse, error) {
	textSearchRequest := &maps.TextSearchRequest{
		Query: query,
	}

	return s.gmaps.TextSearch(context.Background(), textSearchRequest)
}

func (s *Service) GetValueQuery(pathUrl string) (string, error) {
	r := regexp.MustCompile(regexQuery)
	results := r.FindAllSubmatch([]byte(pathUrl), -1)
	resultString := string(results[0][1])
	unescape, err := url.QueryUnescape(resultString)

	if err != nil {
		return "", err
	}
	return unescape, nil
}

func check(err error) {
	if err != nil {
		log.Fatalf("fatal error: %s", err)
	}
}
