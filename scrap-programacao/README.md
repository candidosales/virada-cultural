# Scrap

## 1 - Get Places

## 2 - Get Types

## 3 - Get Events

```json
{
  "id":"fc85272a-a9ae-47a5-a0c6-6009999cddb0",
  "name":"Cabaré de Palhaços", // blocotit
  "description":"", // blocoitine
  "started_at":"2018-05-19T19:00:00.000-03:00", // bdireita pdata
  "types":[
      {
        "id":"343d7010-8ea6-4efe-a902-071ed0427026",
        "name":"Circo", // blocoestilo
        "color":"#2ee7b6"
      }
  ],
  "tags":[

  ],
  "place":{
      "id":"1523f0dd-63fa-4e80-adb4-4d3079264e3f",
      "lat":-23.5215268,
      "lng":-46.6964466,
      "name":"Centro Cultural Tendal da Lapa", // bdireita plocal (before /) OR bdireita plocal
      "neighborhood":"Lapa" // bdireita plocal
  },
  "save":false
}
```

Start server

```bash
ruby -run -ehttpd . -p8000
```

Access file: http://localhost:8000/html/page.html

Reference:
- https://medium.com/swlh/increase-your-scraping-speed-with-go-and-colly-the-basics-41038bc3647e
