import { EventShow } from './../../interface/event_show.interface';
import { Component, Input, ChangeDetectionStrategy, ChangeDetectorRef, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-events-list',
    templateUrl: './events-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventsListComponent implements OnInit {
    @Input() public events: Observable<any>;

    constructor(private cd: ChangeDetectorRef) {}

    ngOnInit() {
        this.events.subscribe(() => {
            this.cd.markForCheck();
        });
    }

    public trackById(index, event: EventShow) {
        return event.id;
    }
}
