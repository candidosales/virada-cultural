import { EventService } from '../../providers/event.service';
import { EventShow } from '../../interface/event_show.interface';
import { ChangeDetectionStrategy, Input, Component } from '@angular/core';
import { EventType } from '../../interface/event_type.interface';
import { EventTag } from '../../interface/event_tag.interface';
@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-event-types-tags',
    templateUrl: './event-types-tags.component.html',
    styleUrls: ['./event-types-tags.component.scss'],
})
export class EventTypesTagsComponent {
    @Input() public event: EventShow;

    constructor(private eventService: EventService) {}
}
