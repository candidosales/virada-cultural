import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

import { GoogleAnalyticsService } from '../../providers/google-analytics.service';
import { EventShow } from './../../interface/event_show.interface';
import { EventService } from './../../providers/event.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-event-item',
    templateUrl: './event-item.component.html',
})
export class EventItemComponent {
    @Input() public eventItem: EventShow;
    @Input() public showStart = false;

    constructor(
        private eventService: EventService,
        private router: Router,
        public snackBar: MatSnackBar,
        private ga: GoogleAnalyticsService,
        private translate: TranslateService
    ) {}

    async save(eventItem: EventShow) {
        this.ga.sendEvent('event-item-save', 'click', `${eventItem.id}-${eventItem.name}`);
        eventItem.save = true;
        const isSaved = await this.eventService.updateSave(eventItem);
        if (isSaved) {
            this.showSnackBar();
        }
    }

    async removeSave(eventItem) {
        this.ga.sendEvent('event-item-remove', 'click', `${eventItem.id}-${eventItem.name}`);
        eventItem.save = false;
        await this.eventService.updateSave(eventItem);
    }

    public showSnackBar() {
        this.snackBar.open(this.translate.instant('event.addedYourSchedule'), 'OK', {
            duration: 2000,
        });
    }

    public goToEvent(eventItem: EventShow) {
        if (eventItem.id) {
            this.ga.sendEvent('event-item-details', 'click', `${eventItem.id}-${eventItem.name}`);
            this.router.navigate(['/event'], { queryParams: { id: eventItem.id, origin: this.router.url } });
        }
    }
}
