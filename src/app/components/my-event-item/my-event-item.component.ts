import { EventService } from './../../providers/event.service';
import { GoogleAnalyticsService } from './../../providers/google-analytics.service';
import { EventShow } from './../../interface/event_show.interface';
import { Router } from '@angular/router';
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    changeDetection: ChangeDetectionStrategy.OnPush,
    selector: 'app-my-event-item',
    templateUrl: './my-event-item.component.html',
})
export class MyEventItemComponent {
    @Input() public eventItem: EventShow;
    @Input() public onlySave = false;
    constructor(private router: Router, private eventService: EventService, private ga: GoogleAnalyticsService) {}

    public goToEvent(eventItem: EventShow) {
        if (eventItem.id) {
            this.ga.sendEvent('event-item-details', 'click', `${eventItem.id}-${eventItem.name}`);
            const originUrl = this.router.url.substring(0, this.router.url.indexOf('?'));
            this.router.navigate(['/event'], { queryParams: { id: eventItem.id, origin: originUrl } });
        }
    }

    async removeSave(eventItem) {
        this.ga.sendEvent('event-item-remove', 'click', `${eventItem.id}-${eventItem.name}`);
        eventItem.save = false;
        await this.eventService.updateSave(eventItem);
    }
}
