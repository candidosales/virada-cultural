import { Component, EventEmitter, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

import { EventPlace } from '../../interface/event_place.interface';
import { EventTag } from '../../interface/event_tag.interface';
import { EventType } from '../../interface/event_type.interface';
import { EventService } from '../../providers/event.service';
import { GoogleAnalyticsService } from '../../providers/google-analytics.service';
import { Constants } from '../../utils/constants';

@Component({
    selector: 'app-attraction-filters',
    templateUrl: 'attraction-filters.component.html',
})
export class AttractionFiltersComponent implements OnInit {
    public listFilters: any;
    public selectedOptionsPlaces = [];
    public selectedOptionsTypes = [];
    public selectedOptionsTags = [];

    public onFilters: EventEmitter<any> = new EventEmitter();

    constructor(
        private eventService: EventService,
        private ga: GoogleAnalyticsService,
        private bottomSheetRef: MatBottomSheetRef<AttractionFiltersComponent>
    ) {}

    ngOnInit() {
        setTimeout(() => {
            this.listFilters = this.eventService.getListFilter();
        }, 100);
    }

    close(): void {
        this.bottomSheetRef.dismiss();
    }

    clearFilters() {
        this.selectedOptionsTags = [];
        this.selectedOptionsTypes = [];
        this.selectedOptionsPlaces = [];
        localStorage.removeItem(Constants.KEY_CACHE_FILTERS);

        this.listFilters.types.forEach((type) => {
            type.selected = false;
        });

        this.listFilters.tags.forEach((tag) => {
            tag.selected = false;
        });

        this.listFilters.places.forEach((place) => {
            place.selected = false;
        });

        const filter = {
            tags: this.selectedOptionsTags,
            types: this.selectedOptionsTypes,
            places: this.selectedOptionsPlaces,
        };

        this.sendFilterEvent(filter);
    }

    hasFilters() {
        return this.selectedOptionsTypes.length || this.selectedOptionsTags.length || this.selectedOptionsPlaces.length;
    }

    onTypeChange(type: any) {
        type.selected = type.selected === true ? false : true;
        this.filterEvents();
    }

    onTagChange(tag: any) {
        tag.selected = tag.selected === true ? false : true;
        this.filterEvents();
    }

    onPlaceChange(place: any) {
        place.selected = place.selected === true ? false : true;
        this.filterEvents();
    }

    filterEvents() {
        if (this.selectedOptionsTypes.length || this.selectedOptionsTags.length || this.selectedOptionsPlaces.length) {
            this.sendAnalyticsEventsFilters();
        }

        const filter = {
            tags: this.selectedOptionsTags,
            types: this.selectedOptionsTypes,
            places: this.selectedOptionsPlaces,
        };

        this.sendFilterEvent(filter);
    }

    sendFilterEvent(filter) {
        localStorage.setItem(Constants.KEY_CACHE_FILTERS, JSON.stringify(filter));
        this.onFilters.emit(filter);
    }

    sendAnalyticsEventsFilters() {
        if (this.selectedOptionsTypes.length > 0) {
            const typesFilters = this.selectedOptionsTypes.map((type) => type.label).join('-');
            this.ga.sendEvent('schedule-filter', 'click', `filter-types-${typesFilters}`);
        }

        if (this.selectedOptionsTags.length > 0) {
            const tagsFilters = this.selectedOptionsTags.map((tag) => tag.label).join('-');
            this.ga.sendEvent('schedule-filter', 'click', `filter-tags-${tagsFilters}`);
        }

        if (this.selectedOptionsPlaces.length > 0) {
            const placesFilters = this.selectedOptionsPlaces.map((place) => place.label).join('-');
            console.log('placesFilters', placesFilters);
            this.ga.sendEvent('schedule-filter', 'click', `filter-places-${placesFilters}`);
        }
    }

    public trackByTypeId(index, type: EventType) {
        return type.id;
    }

    public trackByTagId(index, tag: EventTag) {
        return tag.id;
    }

    public trackByPlaceId(index, place: EventPlace) {
        return place.id;
    }

    public applyFilter() {
        this.filterEvents();
        this.close();
    }

    get hasTags(): boolean {
        return this.listFilters.tags.length > 0 || false;
    }
}
