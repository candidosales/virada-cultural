import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { EventsListComponent } from './../components/events-list/events-list.component';
import { SharedModule } from './../shared/shared.module';
import { AttractionFiltersComponent } from './attraction-filters/attraction-filters.component';
import { AttractionRoutingModule } from './attraction-routing.module';
import { AttractionComponent } from './attraction.component';

@NgModule({
    imports: [SharedModule, AttractionRoutingModule, FormsModule, TranslateModule.forChild()],
    declarations: [AttractionComponent, AttractionFiltersComponent, EventsListComponent],
    providers: [],
})
export class AttractionModule {}
