import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Router } from '@angular/router';
import isEmpty from 'lodash-es/isEmpty';
import { BehaviorSubject, Observable, of } from 'rxjs';

import { EventService } from './../providers/event.service';
import { GoogleAnalyticsService } from './../providers/google-analytics.service';
import { KeysPipe } from './../shared/pipes/keys.pipe';
import { Constants } from './../utils/constants';
import { AttractionFiltersComponent } from './attraction-filters/attraction-filters.component';

@Component({
    selector: 'app-attraction',
    templateUrl: './attraction.component.html',
})
export class AttractionComponent implements OnInit {
    events$$ = new BehaviorSubject<any>({});
    events$ = this.events$$.asObservable();

    // public events: any;
    public eventsFiltered: any;
    public listFilters: any;
    public hasFilters = false;
    public filters = {
        tags: [],
        types: [],
        places: [],
    };

    public bottomRef: MatBottomSheetRef;

    public tabsTotal = 2;
    public selectedIndex = 0;
    constructor(
        private eventService: EventService,
        private router: Router,
        private ga: GoogleAnalyticsService,
        private bottomSheet: MatBottomSheet,
        private keysPipe: KeysPipe,
        private datePipe: DatePipe
    ) {}

    ngOnInit() {
        // console.log('init Schedule');
        // this.events = this.eventService.getEventDates();

        this.events$$.next(this.eventService.getEventDates());

        if (this.hasCacheFilters()) {
            this.filters = JSON.parse(localStorage.getItem(Constants.KEY_CACHE_FILTERS));
            this.filterEvents();
        } else {
            // console.log('!! Não tem cache para filtros !!');
        }
    }

    goToSearch() {
        this.router.navigate(['search']);
    }

    showFilters() {
        this.ga.sendEvent('schedule-filter', 'click', 'open');
        this.bottomRef = this.bottomSheet.open(AttractionFiltersComponent);
        this.bottomRef.instance.onFilters.subscribe((data) => {
            console.log('showFilters data', data);
            this.filters = data;
        });

        this.bottomRef.afterDismissed().subscribe(() => {
            // this.events = false;
            this.events$$.next({});
            this.filterEvents();
        });
    }

    clearFilters() {
        this.eventService.getFormated().then((events) => {
            this.events$$.next(events);
        });

        this.hasFilters = false;
        localStorage.removeItem(Constants.KEY_CACHE_FILTERS);

        this.filters.types.forEach((type) => {
            type.selected = false;
        });

        this.filters.tags.forEach((tag) => {
            tag.selected = false;
        });

        this.filters.places.forEach((place) => {
            place.selected = false;
        });
    }

    hasCacheFilters(): boolean {
        return !(localStorage.getItem(Constants.KEY_CACHE_FILTERS) === null);
    }

    filterEvents() {
        if (!this.filters.types.length && !this.filters.tags.length && !this.filters.places.length) {
            this.clearFilters();
            return;
        }

        this.hasFilters = true;
        // this.events = await this.eventService.filterEventsFormated(this.filters);

        this.eventService.filterEventsFormated(this.filters).then((events) => {
            this.events$$.next(events);
        });

        // console.log('filterEvents this.events', this.events);
    }

    turnObservable(object: any): Observable<any> {
        return of(object);
    }

    turnArray(object): Array<any> {
        return this.keysPipe.transform(object);
    }

    // showLoading() {
    //     return !this.events;
    // }

    trackByDate(index, date: any) {
        return date.key;
    }

    hasEvents(value: any): boolean {
        return !isEmpty(value);
    }
}
