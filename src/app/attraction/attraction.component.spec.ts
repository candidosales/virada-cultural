import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AttractionComponent } from './attraction.component';

describe('ScheduleComponent', () => {
    let component: AttractionComponent;
    let fixture: ComponentFixture<AttractionComponent>;

    beforeEach(
        waitForAsync(() => {
            TestBed.configureTestingModule({
                declarations: [AttractionComponent],
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(AttractionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
