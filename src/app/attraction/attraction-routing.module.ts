import { AttractionComponent } from './attraction.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const AttractionRoutes: Routes = [
    {
        path: '',
        component: AttractionComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(AttractionRoutes)],
    exports: [RouterModule],
})
export class AttractionRoutingModule {}
