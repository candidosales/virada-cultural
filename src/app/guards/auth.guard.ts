import { Constants } from './../utils/constants';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from '@angular/router';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private router: Router) {}

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        return this.checkLogin();
    }

    public canLoad(route: Route): boolean {
        return this.checkLogin();
    }

    private checkLogin() {
        if (localStorage.getItem(Constants.KEY_EVENT_NAME)) {
            // logged in so return true
            return true;
        }

        this.router.navigate(['login']);
        return false;
    }
}
