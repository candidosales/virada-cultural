export class Constants {
    public static get KEY_CACHE_EVENTS_ID(): string {
        return `cache_events_id`;
    }
    public static get KEY_CACHE_EVENTS_FORMATED(): string {
        return `cache_events_formated-${this.getEventId()}`;
    }
    public static get KEY_CACHE_EVENTS_ORIGINAL(): string {
        return `cache_events_original-${this.getEventId()}`;
    }
    public static get KEY_CACHE_HOME_SHOW_INFO(): string {
        return `cache_home_show_info-${this.getEventId()}`;
    }
    public static get KEY_CACHE_DIALOG_ONBOARDING_SHOW_INFO(): string {
        return `cache_dialog_onboarding_show_info-${this.getEventId()}`;
    }
    public static get KEY_EVENT_NAME(): string {
        return this.getEventId();
    }
    public static get KEY_CACHE_FILTERS(): string {
        return `cache_filters-${this.getEventId()}`;
    }
    public static get KEY_CACHE_THEME(): string {
        return `cache_theme-${this.getEventId()}`;
    }

    public static getEventId(): string {
        return localStorage.getItem(this.KEY_CACHE_EVENTS_ID) || '';
    }
}
