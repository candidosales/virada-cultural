import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';
import { MatSnackBar } from '@angular/material/snack-bar';

import { GoogleAnalyticsService } from '../providers/google-analytics.service';
import { EventService } from './../providers/event.service';
import * as styleMap from './../utils/styleMap.json';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
})
export class MapComponent implements OnDestroy, OnInit {
    @ViewChild(GoogleMap, { static: false }) map: GoogleMap;
    @ViewChild(MapInfoWindow, { static: false }) info: MapInfoWindow;

    public places: Array<any>;
    public markers = [];
    public styleMap = styleMap;
    public currentLocation = {
        lat: -23.5612836,
        lng: -46.6599414,
    };
    public zoom = 13;
    public placeSelected;

    public geoOptions = {
        enableHighAccuracy: true,
        // maximumAge        : 30000,
        // timeout           : 27000
    };

    center: google.maps.LatLngLiteral;
    options: google.maps.MapOptions = {
        zoomControl: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        maxZoom: 15,
        minZoom: 8,
        styles: styleMap as google.maps.MapTypeStyle[],
    };
    infoContent = '';

    public watchGeolocationID;

    constructor(private eventService: EventService, private ga: GoogleAnalyticsService, public snackBar: MatSnackBar) {
        if ('geolocation' in navigator) {
            /* geolocation is available */
            this.watchGeolocation();
        } else {
            /* geolocation IS NOT available */
            this.snackBar.open('A geolocalizacão não está disponível');
        }
    }

    ngOnInit() {
        navigator.geolocation.getCurrentPosition((position) => {
            this.center = {
                lat: this.currentLocation.lat,
                lng: this.currentLocation.lng,
            };
        });

        this.getEventsMap();
    }

    openInfo(marker: MapMarker, content) {
        this.infoContent = content;
        this.info.open(marker);
    }

    clickedMarker(placeSelect: any) {
        if (!this.placeSelected || placeSelect.id !== this.placeSelected.id) {
            // if (this.placeSelected) {
            //   this.places[this.placeSelected.id].active = false;
            // }

            this.places.forEach((place) => {
                place.active = false;
            });

            this.places.forEach((place) => {
                if (placeSelect.id === place.id) {
                    place.active = true;
                }
            });

            // this.places[placeSelect.id].active = true;
            this.placeSelected = placeSelect;
        }
    }

    getEventsMap() {
        const event = this.eventService.getEvent();
        this.places = event.places;
        this.convertToMarkers(this.places);
    }

    convertToMarkers(places: any) {
        // TODO add markers in Map
        console.log('convertToMarkers this.places', places);
        if (places.length > 0) {
            // this.markers = [];

            places.forEach((value, key, map) => {
                this.markers.push({
                    position: {
                        lat: value.lat,
                        lng: value.lng,
                    },
                    label: {
                        color: 'red',
                        text: 'Marker label ' + (this.markers.length + 1),
                    },
                    title: value.name,
                    options: { animation: google.maps.Animation.BOUNCE },
                });
                console.log('this.markers', this.markers);
            });
        }
        console.log('this.markers', this.markers);
    }

    getCurrentLocation() {
        // console.log('getCurrentLocation');
        navigator.geolocation.getCurrentPosition((position) => {
            this.currentLocation.lat = position.coords.latitude;
            this.currentLocation.lng = position.coords.longitude;
        }, this.errorGeolocation);
    }

    watchGeolocation() {
        // console.log('watchGeolocation');
        this.watchGeolocationID = navigator.geolocation.watchPosition(
            (position) => {
                this.ga.sendLocation(position.coords.latitude, position.coords.longitude);

                this.currentLocation.lat = position.coords.latitude;
                this.currentLocation.lng = position.coords.longitude;
            },
            this.errorGeolocation,
            this.geoOptions
        );
    }

    removeWatchGeolocation() {
        navigator.geolocation.clearWatch(this.watchGeolocationID);
    }

    errorGeolocation(error) {
        // console.error('Sorry, no position available.', error.code, error.message);
    }

    successGeolocation(position) {
        // console.log('successGeolocation', position);
    }

    closePlaceSelected() {
        this.placeSelected.active = false;
        this.placeSelected = null;
    }

    ngOnDestroy(): void {
        this.removeWatchGeolocation();
    }

    public goToMap() {
        if (this.placeSelected.latitude && this.placeSelected.longitude) {
            // tslint:disable-next-line:max-line-length
            window.location.replace(
                `https://www.google.com/maps/search/?api=1&query=${this.placeSelected.latitude},${this.placeSelected.longitude}`
            );
        } else {
            this.snackBar.open('Infelizmente este evento não possui localização 🙁', 'OK', {
                duration: 3000,
            });
        }
    }
}
