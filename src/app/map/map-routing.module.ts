import { MapComponent } from './map.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const MapRoutes: Routes = [
    {
        path: '',
        component: MapComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(MapRoutes)],
    exports: [RouterModule],
})
export class MapRoutingModule {}
