import { SharedModule } from './../shared/shared.module';
import { CustomMaterialModule } from './../shared/custom-material.module';
import { MapComponent } from './map.component';
import { MapRoutingModule } from './map-routing.module';
import { NgModule } from '@angular/core';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
    imports: [SharedModule, CustomMaterialModule, MapRoutingModule, GoogleMapsModule],
    declarations: [MapComponent],
    providers: [],
})
export class MapModule {}
