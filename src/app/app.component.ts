import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ThemeService } from './theme/theme.service';

@Component({
    selector: 'app-root',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    constructor(private router: Router, private themeService: ThemeService, private translate: TranslateService) {
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                (<any>window).ga('set', 'page', event.urlAfterRedirects);
                (<any>window).ga('send', 'pageview');
            }
        });
        translate.addLangs(['en', 'pt', 'es']);
        const browserLang = translate.getBrowserLang();
        translate.setDefaultLang(browserLang.match(/en|pt|es/) ? browserLang : 'en');
    }

    ngOnInit() {
        const activeTheme = this.themeService.getCacheTheme();
        this.themeService.setTheme(activeTheme);
    }
}
