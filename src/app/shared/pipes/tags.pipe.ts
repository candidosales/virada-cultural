import { EventService } from '../../providers/event.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'tag' })
export class TagsPipe implements PipeTransform {
    constructor(private eventService: EventService) {}

    transform(tagId: string): string {
        return this.eventService.getEventTag(tagId).name;
    }
}
