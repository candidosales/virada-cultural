import { EventService } from '../../providers/event.service';
import { Pipe, PipeTransform } from '@angular/core';
import { EventType } from '../../interface/event_type.interface';

@Pipe({ name: 'type' })
export class TypesPipe implements PipeTransform {
    constructor(private eventService: EventService) {}

    transform(typeId: string): EventType {
        return this.eventService.getEventType(typeId);
    }
}
