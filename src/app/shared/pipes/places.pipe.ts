import { EventService } from '../../providers/event.service';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'place' })
export class PlacesPipe implements PipeTransform {
    constructor(private eventService: EventService) {}

    transform(placeId: string): string {
        const place = this.eventService.getEventPlace(placeId);
        if (place) {
            return `${place.name} , ${place.neighborhood}`;
        }
        return '';
    }
}
