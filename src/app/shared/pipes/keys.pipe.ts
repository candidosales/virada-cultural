import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'keys' })
export class KeysPipe implements PipeTransform {
    transform(value, args?: string[]): Array<any> {
        const keys = [];

        if (value) {
            for (const key in value) {
                if (value.hasOwnProperty(key)) {
                    keys.push({ key: key, value: value[key] });
                }
            }
        }

        return keys;
    }
}
