import { Pipe, PipeTransform } from '@angular/core';
import memo from 'memo-decorator';

@Pipe({
    name: 'textFilters',
    pure: true,
})
export class TextFiltersPipe implements PipeTransform {
    transform(filters) {
        // Transformations...
        return this.getTextFilters(filters);
    }

    @memo()
    getTextFilters(filters) {
        // console.log('Calling getRelativeDate method inside the pipe');
        let text = '';

        if (filters.types.length) {
            text += filters.types.map((type) => type.label).join(', ');
        }

        if (filters.tags.length) {
            const tags = filters.tags.map((tag) => tag.label).join(', ');

            text += filters.types.length ? ', ' : '';
            text += tags;
        }

        if (filters.places.length) {
            const places = filters.places.map((place) => place.label).join(', ');

            text += filters.types.length ? ', ' : '';
            text += filters.tags.length ? ', ' : '';
            text += places;
        }

        return text;
    }
}
