import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EventItemComponent } from '../components/event-item/event-item.component';
import { EventTypesTagsComponent } from '../components/event-types-tags/event-types-tags.component';
import { CustomMaterialModule } from './custom-material.module';
import { KeysPipe } from './pipes/keys.pipe';
import { PlacesPipe } from './pipes/places.pipe';
import { TagsPipe } from './pipes/tags.pipe';
import { TextFiltersPipe } from './pipes/text-filters.pipe';
import { TypesPipe } from './pipes/types.pipe';

@NgModule({
    declarations: [
        EventItemComponent,
        EventTypesTagsComponent,
        KeysPipe,
        TextFiltersPipe,
        PlacesPipe,
        TypesPipe,
        TagsPipe,
    ],
    imports: [CommonModule, CustomMaterialModule],
    exports: [
        CommonModule,
        CustomMaterialModule,
        EventItemComponent,
        EventTypesTagsComponent,
        KeysPipe,
        TextFiltersPipe,
        PlacesPipe,
        TypesPipe,
        TagsPipe,
    ],
    providers: [KeysPipe, TextFiltersPipe],
})
export class SharedModule {}
