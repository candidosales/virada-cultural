import { NgModule } from '@angular/core';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
    imports: [
        MatButtonModule,
        MatSnackBarModule,
        MatIconModule,
        MatTabsModule,
        MatListModule,
        MatInputModule,
        MatExpansionModule,
        MatMenuModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatBottomSheetModule,
        MatSlideToggleModule,
    ],
    exports: [
        MatButtonModule,
        MatSnackBarModule,
        MatIconModule,
        MatTabsModule,
        MatListModule,
        MatInputModule,
        MatExpansionModule,
        MatMenuModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatBottomSheetModule,
        MatSlideToggleModule,
    ],
})
export class CustomMaterialModule {}
