import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { addHours, format, parseISO } from 'date-fns';

import { EventShow } from '../interface/event_show.interface';
import { EventService } from './../providers/event.service';
import { GoogleAnalyticsService } from './../providers/google-analytics.service';
import { ShareService } from './../providers/share.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
})
export class EventComponent implements OnInit, OnDestroy {
    public event: EventShow;
    public originRouter;
    private subscribeRouter: any;

    constructor(
        private eventService: EventService,
        private shareService: ShareService,
        private router: Router,
        private route: ActivatedRoute,
        public snackBar: MatSnackBar,
        private datePipe: DatePipe,
        private ga: GoogleAnalyticsService,
        private translate: TranslateService
    ) {}

    ngOnInit(): void {
        this.subscribeRouter = this.route.queryParams.subscribe((params) => {
            this.event = this.eventService.getEventById(params['id']);
            this.originRouter = params['origin'];
            // In a real app: dispatch action to load the details here.
        });
    }

    async save(eventItem) {
        this.ga.sendEvent('event-save', 'click', `${eventItem.id}-${eventItem.name}`);
        eventItem.save = true;
        const isSaved = await this.eventService.updateSave(eventItem);
        if (isSaved) {
            this.showSnackBar();
        }
    }

    async removeSave(eventItem) {
        this.ga.sendEvent('event-remove', 'click', `${eventItem.id}-${eventItem.name}`);
        eventItem.save = false;
        await this.eventService.updateSave(eventItem);
    }

    public showSnackBar() {
        this.snackBar.open(this.translate.instant('event.addedYourSchedule'), 'OK', {
            duration: 2000,
        });
    }

    public goToMap() {
        this.ga.sendEvent('event', 'click', 'map');
        if (this.event.place.lat && this.event.place.lng) {
            window.location.replace(
                `https://www.google.com/maps/search/?api=1&query=${this.event.place.lat},${this.event.place.lng}`
            );
        } else {
            this.snackBar.open(this.translate.instant('event.unfortunatelyEventHasNoLocation'), 'OK', {
                duration: 3000,
            });
        }
    }

    public share() {
        this.ga.sendEvent('event', 'share', `${this.event.id}-${this.event.name}`);
        const url = window.location.href;
        const date = this.datePipe.transform(this.event.started_at, 'd/MMMM - HH:mm (EEEE)');

        this.shareService.share(url, this.event.name, `Veja o evento ${this.event.name} que vai ocorrer no ${date}`);
    }

    get hasLocation() {
        return (this.event.place && this.event.place.lat && this.event.place.lng) || false;
    }

    addCalendar() {
        const text = encodeURI(this.event.name);
        const details = encodeURI(this.event.description);
        const location = encodeURI(`${this.event.place.name} - ${this.event.place.neighborhood}`);

        const dateStart = format(parseISO(this.event.started_at), 'yyyyMMddTHHmm00');
        const dateEnd = format(addHours(parseISO(this.event.started_at), 1), 'yyyyMMddTHHmm00');

        const dates = `${dateStart}/${dateEnd}`;
        // tslint:disable-next-line:max-line-length
        const url = `https://www.google.com/calendar/render?action=TEMPLATE&text=${text}&dates=${dates}&details=${details}&location=${location}&sf=true&output=xml`;
        window.location.replace(url);
    }

    back() {
        this.router.navigate([this.originRouter]);
    }

    ngOnDestroy() {
        this.subscribeRouter.unsubscribe();
    }
}
