import { DatePipe, registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localePt from '@angular/common/locales/pt';
import localeEs from '@angular/common/locales/es';

import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventComponent } from './event/event.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login/login.component';
import { NoContentComponent } from './no-content';
import { EventService } from './providers/event.service';
import { SettingsService } from './providers/settings.service';
import { SharedModule } from './shared/shared.module';
import { darkTheme } from './theme/dark-theme';
import { lightTheme } from './theme/light-theme';
import { ThemeModule } from './theme/theme.module';

export function eventsProviderFactory(eventService: EventService) {
    return () => eventService.getEventsInServer();
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

registerLocaleData(localePt, 'pt');
registerLocaleData(localeEs, 'es');

@NgModule({
    bootstrap: [AppComponent],
    declarations: [AppComponent, HomeComponent, NoContentComponent, LoginComponent, EventComponent],
    /**
     * Import Angular's modules.
     */
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        SharedModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
        ThemeModule.forRoot({
            themes: [lightTheme, darkTheme],
            active: 'light',
        }),
        NgbModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
    ],
    /**
     * Expose our Services and Providers into Angular's dependency injection.
     */
    providers: [
        DatePipe,
        {
            provide: LOCALE_ID,
            deps: [SettingsService],
            useFactory: (settingsService) => settingsService.getLanguage(),
        },
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: HammerGestureConfig,
        },
        EventService,
        {
            provide: APP_INITIALIZER,
            useFactory: eventsProviderFactory,
            deps: [EventService],
            multi: true,
        },
    ],
})
export class AppModule {}
