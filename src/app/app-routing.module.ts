import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { EventComponent } from './event/event.component';
import { AuthGuard } from './guards/auth.guard';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { NoContentComponent } from './no-content';

const ROUTES: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        children: [
            {
                path: 'my-agenda',
                loadChildren: () => import('./my-agenda/my-agenda.module').then((m) => m.MyAgendaModule),
            },
            {
                path: 'attraction',
                loadChildren: () => import('./attraction/attraction.module').then((m) => m.AttractionModule),
            },
            { path: 'map', loadChildren: () => import('./map/map.module').then((m) => m.MapModule) },
            { path: 'info', loadChildren: () => import('./info/info.module').then((m) => m.InfoModule) },
            { path: 'search', loadChildren: () => import('./search/search.module').then((m) => m.SearchModule) },
        ],
    },
    {
        path: 'event',
        component: EventComponent,
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '',
        redirectTo: '/home/my-agenda',
        pathMatch: 'full',
    },
    {
        path: '**',
        component: NoContentComponent,
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(ROUTES, {
            preloadingStrategy: PreloadAllModules,
            scrollPositionRestoration: 'enabled',
            relativeLinkResolution: 'legacy',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
