import { EventPlace } from './event_place.interface';
import { EventTag } from './event_tag.interface';
import { EventType } from './event_type.interface';
export interface EventConcert {
    id: string;
    checksum: string;
    name: string;
    description: any;
    dates: Array<any>;
    telephones: any;
    places: Array<EventPlace>;
    types: Array<EventType>;
    tags: Array<EventTag>;
}
