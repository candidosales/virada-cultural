import { EventShow } from './event_show.interface';

export interface EventAttractionDay {
    [propName: string]: EventAttractionHour;
}

export interface EventAttractionHour {
    [propName: string]: Array<EventShow>;
}
