import { EventPlace } from './event_place.interface';
import { EventType } from './event_type.interface';
import { EventTag } from './event_tag.interface';
export interface EventShow {
    id: string;
    name: string;
    description: string;
    place: EventPlace;
    started_at: string;
    save: boolean;
    tags: Array<EventTag>;
    types: Array<EventType>;
}
