export interface EventPlace {
    id: string;
    name: string;
    neighborhood: string;
    lat: any;
    lng: any;
}
