export interface EventType {
    id: string;
    name: string;
    color?: string;
}
