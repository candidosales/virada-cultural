import { EventShow } from './event_show.interface';
export interface EventDate {
    date: string;
    events: Array<EventShow>;
}
