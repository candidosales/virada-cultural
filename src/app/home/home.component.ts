import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { GoogleAnalyticsService } from './../providers/google-analytics.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
    // public tabLinks: Array<any> = [{
    //   label: 'Agenda',
    //   icon: 'person_pin',
    //   link: 'my-agenda',
    //   active: true
    // }, {
    //   label: 'Atrações',
    //   icon: 'access_time',
    //   link: 'schedule',
    //   active: false
    // }, {
    //   label: 'Busca',
    //   icon: 'search',
    //   link: 'search',
    //   active: false
    // }, {
    //   label: 'Mapa',
    //   icon: 'map',
    //   link: 'map',
    //   active: false
    // }, {
    //   label: 'Info',
    //   icon: 'info_outline',
    //   link: 'info',
    //   active: false
    // }];

    public tabLinks: Array<any>;
    constructor(private ga: GoogleAnalyticsService, private translateService: TranslateService) {}

    ngOnInit() {
        this.tabLinks = [
            {
                label: this.translateService.instant('home.schedule'),
                icon: 'event',
                link: 'my-agenda',
                active: true,
            },
            {
                label: this.translateService.instant('home.attractions'),
                icon: 'local_play',
                link: 'attraction',
                active: false,
            },
            {
                label: this.translateService.instant('home.search'),
                icon: 'search',
                link: 'search',
                active: false,
            },
            {
                label: this.translateService.instant('home.info'),
                icon: 'info_outline',
                link: 'info',
                active: false,
            },
        ];
    }

    sendEvent(label: string) {
        this.ga.sendEvent('nav-bottom', 'click', label);
    }
}
