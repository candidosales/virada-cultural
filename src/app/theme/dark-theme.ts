import { Theme } from './symbols';

export const darkTheme: Theme = {
    name: 'dark',
    properties: {
        '--background': 'hsl(200 10% 13%)',
        '--on-background': 'hsl(200 10% 80%)',
        '--primary': 'hsl(214 40% 55%)',
        '--on-primary': '#fff',
        '--secondary': '#2b3033',
        '--on-secondary': '#ffffffe0',
        '--surface': 'hsl(200 10% 10%)',
        '--on-surface': '#dddede',
        '--error': '#E74E3C',
        '--on-error': '#fff',
        '--border': 'hsl(200 10% 40%)',
        '--border-content': 'hsl(200 10% 16%)',
    },
};
