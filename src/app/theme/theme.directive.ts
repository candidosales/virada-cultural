import { Directive, OnInit, OnDestroy, ElementRef, Input, Inject } from '@angular/core';
import { ThemeService } from './theme.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Theme } from './symbols';
import { DOCUMENT } from '@angular/common';

@Directive({
    selector: '[theme]',
})
export class ThemeDirective implements OnInit, OnDestroy {
    @Input() scoped = false;
    private _destroy$ = new Subject();

    constructor(
        private _elementRef: ElementRef,
        private _themeService: ThemeService,
        @Inject(DOCUMENT) private _document: any
    ) {}

    ngOnInit() {
        const active = this._themeService.getActiveTheme();
        if (active) {
            this.updateTheme(active);
        }

        this._themeService.themeChange
            .pipe(takeUntil(this._destroy$))
            .subscribe((theme: Theme) => this.updateTheme(theme));
    }

    ngOnDestroy() {
        this._destroy$.next('');
        this._destroy$.complete();
    }

    updateTheme(theme: Theme) {
        const element = this.getElement();
        // project properties onto the element
        for (const key in theme.properties) {
            if (theme.properties.hasOwnProperty(key)) {
                element.style.setProperty(key, theme.properties[key]);
            }
        }
        const name = this._themeService.theme;
        // remove old theme
        const classRemove = name === 'light' ? 'dark' : 'light';
        element.classList.remove(`${classRemove}-theme`);

        // alias element with theme name
        element.classList.add(`${name}-theme`);
    }

    getElement() {
        return this.scoped ? this._elementRef.nativeElement : this._document.body;
    }
}
