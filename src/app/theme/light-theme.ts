import { Theme } from './symbols';

export const lightTheme: Theme = {
    name: 'light',
    properties: {
        '--background': '#f6f7f9',
        '--on-background': '#4A4A4A',
        '--primary': '#3C71D0',
        '--on-primary': '#fff',
        '--secondary': '#f6f7f9',
        '--on-secondary': '#95989A',
        '--surface': '#fff',
        '--on-surface': '#000',
        '--error': '#E74E3C',
        '--on-error': '#fff',
        '--border': '#c4c4c4',
        '--border-content': '#f6f7f9',
    },
};
