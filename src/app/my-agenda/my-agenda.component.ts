import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';

import { EventService } from './../providers/event.service';
import { GoogleAnalyticsService } from './../providers/google-analytics.service';
import { ShareService } from './../providers/share.service';
import { Constants } from './../utils/constants';
import { DialogExitComponent } from './dialog-exit/dialog-exit.component';
import { DialogOnboardingComponent } from './dialog-onboarding/dialog-onboarding.component';
import { DialogPromptIosComponent } from './dialog-prompt-ios/dialog-prompt-ios.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-my-agenda',
    templateUrl: './my-agenda.component.html',
})
export class MyAgendaComponent implements OnInit, OnDestroy {
    public hasEventsSaved = false;
    public showInfo = false;
    public events: any;
    public addHomePrompt: any;
    private subscribeRouter: any;
    public loading = true;

    public deferredPrompt;
    public showInstallButton = false;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public dialog: MatDialog,
        private eventService: EventService,
        private shareService: ShareService,
        public snackBar: MatSnackBar,
        private ga: GoogleAnalyticsService,
        private translate: TranslateService
    ) {}

    async ngOnInit() {
        this.showInfo = this.getCacheHomeShowInfo();

        this.events = await this.eventService.getMyAgenda();
        this.hasEventsSaved = await this.eventService.hasEventSaved();
        this.loading = this.hasEventsSaved === false ? false : true;

        if (this.getCacheDialogOnboarding()) {
            this.showDialogOnboarding();
        }

        this.subscribeRouter = this.route.queryParams.pipe(take(1)).subscribe((params) => {
            if (params['eventsImport'] && params['eventsImport'].length > 0) {
                this.importEvents(params['eventsImport'].split(','));
            }
        });

        // PWA propmpt
        window.addEventListener('beforeinstallprompt', (e) => {
            // Prevent Chrome 67 and earlier from automatically showing the prompt
            e.preventDefault();
            // Stash the event so it can be triggered later.
            this.deferredPrompt = e;

            this.showInstallButton = true;
        });

        this.showInstallButton = this.isIOS();
    }

    verifyHasEvents() {}

    goToSchedule() {
        this.router.navigate(['home/attraction']);
    }

    removeInfo() {
        this.showInfo = false;
        localStorage.setItem(Constants.KEY_CACHE_HOME_SHOW_INFO, false.toString());
    }

    getCacheHomeShowInfo() {
        return localStorage.getItem(Constants.KEY_CACHE_HOME_SHOW_INFO) === 'false' ? false : true || true;
    }

    getCacheDialogOnboarding() {
        return localStorage.getItem(Constants.KEY_CACHE_DIALOG_ONBOARDING_SHOW_INFO) === 'false' ? false : true || true;
    }

    showDialogOnboarding() {
        const dialogRef: MatDialogRef<DialogOnboardingComponent> = this.dialog.open(DialogOnboardingComponent, {
            width: '400px',
        });

        dialogRef.afterClosed().subscribe((result) => {
            localStorage.setItem(Constants.KEY_CACHE_DIALOG_ONBOARDING_SHOW_INFO, false.toString());
        });
    }

    shareMyAgenda() {
        const eventsSavedIds: Array<string> = [];

        if (this.events) {
            for (const date in this.events) {
                // tslint:disable-next-line:forin
                for (const hour in this.events[date]) {
                    // tslint:disable-next-line:max-line-length
                    for (const event of this.events[date][hour]) {
                        if (event.save) {
                            eventsSavedIds.push(event.id);
                        }
                    }
                }
            }
        }

        if (eventsSavedIds.length > 0) {
            this.share(eventsSavedIds);
        }
    }

    public share(eventsSavedIds: Array<string>) {
        const url = `${window.location.href}?eventsImport=${eventsSavedIds.join(',')}`;
        this.ga.sendEvent('my-agenda-share', 'click', `share-${eventsSavedIds.join(',')}`);
        this.shareService.share(
            url,
            this.translate.instant('share.shareMyCalendar'),
            this.translate.instant('share.importMyCalendar') + ' Carnaval SP 2019'
        );
    }

    get showLoading() {
        return !this.hasEventsSaved && this.loading;
    }

    get showBanner() {
        return !this.hasEventsSaved && !this.loading;
    }

    logout() {
        this.ga.sendEvent('my-agenda', 'click', 'logout');

        localStorage.removeItem(Constants.KEY_CACHE_DIALOG_ONBOARDING_SHOW_INFO);
        localStorage.removeItem(Constants.KEY_CACHE_HOME_SHOW_INFO);
        localStorage.removeItem(Constants.KEY_EVENT_NAME);
        location.reload();
    }

    showDialogExit() {
        const dialogRef: MatDialogRef<DialogExitComponent> = this.dialog.open(DialogExitComponent, {
            width: '400px',
        });

        dialogRef
            .afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                if (result) {
                    this.logout();
                }
            });
    }

    importEvents(eventsImport) {
        this.eventService.saveEvents(eventsImport);

        setTimeout(async () => {
            this.snackBar.open(this.translate.instant('my-agenda.theCalendarBeenImported') + ' 🎉', 'OK', {
                duration: 10000,
            });

            this.events = await this.eventService.getFormated();
            this.hasEventsSaved = await this.eventService.hasEventSaved();

            // const url = this.removeParam('eventsImport', window.location.href);
            // console.log(url);
        }, 1000);
    }

    removeParam(key, sourceURL) {
        let rtn = sourceURL.split('?')[0],
            param,
            params_arr = [];
        const queryString = sourceURL.indexOf('?') !== -1 ? sourceURL.split('?')[1] : '';
        if (queryString !== '') {
            params_arr = queryString.split('&');
            for (let i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split('=')[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + '?' + params_arr.join('&');
        }
        return rtn.replace('?', '');
    }

    install() {
        this.ga.sendEvent('my-agenda', 'click', `install`);

        if (this.isIOS) {
            this.showDialogPromptIOS();
        }

        if (this.deferredPrompt) {
            // Show the prompt
            this.deferredPrompt.prompt();
            // Wait for the user to respond to the prompt
            this.deferredPrompt.userChoice.then((choiceResult) => {
                if (choiceResult.outcome === 'accepted') {
                    this.ga.sendEvent('my-agenda', 'click', `app-installed`);
                    console.log('User accepted the A2HS prompt');

                    // hide our user interface that shows our A2HS button
                    this.showInstallButton = false;
                } else {
                    console.log('User dismissed the A2HS prompt');
                    this.ga.sendEvent('my-agenda', 'click', `app-dismissed`);
                }
                this.deferredPrompt = null;
            });
        }
    }

    showDialogPromptIOS() {
        const dialogRef: MatDialogRef<DialogPromptIosComponent> = this.dialog.open(DialogPromptIosComponent, {
            width: '420px',
            height: '320px',
        });

        dialogRef.afterClosed().subscribe((result) => {});
    }

    isIOS(): boolean {
        return !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
    }

    ngOnDestroy() {
        this.subscribeRouter.unsubscribe();
    }
}
