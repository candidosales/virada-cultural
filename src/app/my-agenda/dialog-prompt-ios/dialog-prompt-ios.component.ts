import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-prompt-ios',
    templateUrl: './dialog-prompt-ios.component.html',
    styleUrls: ['./dialog-prompt-ios.component.scss'],
})
export class DialogPromptIosComponent {
    constructor(public dialogRef: MatDialogRef<DialogPromptIosComponent>) {}
}
