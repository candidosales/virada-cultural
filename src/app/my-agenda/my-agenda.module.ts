import { NgModule } from '@angular/core';

import { MyEventItemComponent } from './../components/my-event-item/my-event-item.component';
import { SharedModule } from './../shared/shared.module';
import { DialogExitComponent } from './dialog-exit/dialog-exit.component';
import { DialogOnboardingComponent } from './dialog-onboarding/dialog-onboarding.component';
import { DialogPromptIosComponent } from './dialog-prompt-ios/dialog-prompt-ios.component';
import { MyAgendaRoutingModule } from './my-agenda-routing.module';
import { MyAgendaComponent } from './my-agenda.component';
import {
    TranslateModule,
    TranslateLoader,
    TranslateCompiler,
    TranslateParser,
    MissingTranslationHandler,
} from '@ngx-translate/core';

@NgModule({
    imports: [SharedModule, MyAgendaRoutingModule, TranslateModule.forChild()],
    declarations: [
        MyAgendaComponent,
        MyEventItemComponent,
        DialogExitComponent,
        DialogOnboardingComponent,
        DialogPromptIosComponent,
    ],
    exports: [MyAgendaComponent, MyEventItemComponent],
    providers: [],
})
export class MyAgendaModule {}
