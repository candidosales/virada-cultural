import { MyAgendaComponent } from './my-agenda.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const MyAgendaRoutes: Routes = [
    {
        path: '',
        component: MyAgendaComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(MyAgendaRoutes)],
    exports: [RouterModule],
})
export class MyAgendaRoutingModule {}
