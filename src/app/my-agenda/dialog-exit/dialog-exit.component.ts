import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-exit',
    templateUrl: './dialog-exit.component.html',
})
export class DialogExitComponent {
    links_buy: any[] = [];

    constructor(public dialogRef: MatDialogRef<DialogExitComponent>) {}
}
