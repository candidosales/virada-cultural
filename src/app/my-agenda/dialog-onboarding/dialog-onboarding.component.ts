import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-onboarding',
    templateUrl: './dialog-onboarding.component.html',
})
export class DialogOnboardingComponent {
    constructor(public dialogRef: MatDialogRef<DialogOnboardingComponent>) {}
}
