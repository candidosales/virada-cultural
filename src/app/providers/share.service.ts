import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root',
})
export class ShareService {
    private navigatorWindow: any;

    constructor(private snackBar: MatSnackBar, private translate: TranslateService) {
        this.navigatorWindow = window.navigator;
    }

    public share(url: string, title: string, text: string) {
        if (this.navigatorWindow && this.navigatorWindow.share) {
            this.navigatorWindow
                .share({
                    title: title,
                    text: text,
                    url: url,
                })
                .then(() => {
                    this.snackBar.open(this.translate.instant('share.successfullyShared'));
                })
                .catch((error) => console.log('Error sharing', error));
        } else {
            this.copyToClipboard(url);
        }
    }

    public copyToClipboard(url: string) {
        document.addEventListener('copy', (e: any) => {
            e.clipboardData.setData('text/plain', url);
            e.preventDefault();
            document.removeEventListener('copy', e);
            this.snackBar.open(this.translate.instant('share.savedYourClipboard'), 'OK', {
                duration: 3000,
            });
        });
        document.execCommand('copy');
    }
}
