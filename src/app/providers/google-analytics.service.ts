import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class GoogleAnalyticsService {
    sendEvent(category: string, action: string, label: string, value?: any) {
        (<any>window).ga('send', 'event', category, action, label);
    }

    sendLocation(latitude: number, longitude: number) {
        const geolocationDatetime = new Date().toISOString();

        (<any>window).ga('set', 'dimension1', latitude + '');
        (<any>window).ga('set', 'dimension2', longitude + '');
        (<any>window).ga('set', 'dimension3', geolocationDatetime + '');

        // console.log(geolocationDatetime);

        (<any>window).ga('send', 'event', 'geolocation', 'send', 'coordinates', {
            dimension1: latitude,
            dimension2: longitude,
            dimension3: geolocationDatetime,
        });
    }
}
