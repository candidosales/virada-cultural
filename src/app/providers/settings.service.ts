import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
    providedIn: 'root',
})
export class SettingsService {
    constructor(private translate: TranslateService) {}

    getLanguage() {
        return this.translate.getBrowserLang();
    }
}
