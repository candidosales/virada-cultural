import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { format, parseISO } from 'date-fns';
import cloneDeep from 'lodash-es/cloneDeep';
import isEmpty from 'lodash-es/isEmpty';
import * as lunr from 'lunr';

import { EventPlace } from '../interface/event_place.interface';
import { EventTag } from '../interface/event_tag.interface';
import { EventType } from '../interface/event_type.interface';
import { EventConcert } from './../interface/event_concert.interface';
import { Constants } from './../utils/constants';
import { EventAttractionDay } from '../interface/attraction.interface';

@Injectable({
    providedIn: 'root',
})
export class EventService {
    public eventDays: any = {};
    public eventDates: any = {};

    public events: EventConcert;
    public indexLunr;
    public listSearch: any = {};
    public listFilter = {
        places: [],
        tags: [],
        types: [],
    };

    constructor(private httpClient: HttpClient) {}

    public async buildEventFormatAndIndexed() {
        this.indexListFilter();
        await this.getFormated();
        await this.buildEventDates();
        this.indexFormatEvents(JSON.parse(localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED)));
    }

    buildEventDates(): Promise<any> {
        return new Promise((resolve) => {
            const event = this.getEvent();
            const size = event.dates.length;
            for (let i = 0; i < size; i++) {
                this.eventDates[event.dates[i].date] = {};
            }
            resolve(this.eventDates);
        });
    }

    public getEventsInServer(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.httpClient.get<EventConcert>('/assets/config/event.min.json').subscribe((result) => {
                if (result) {
                    this.events = result;
                    localStorage.setItem(Constants.KEY_CACHE_EVENTS_ID, this.events.id);
                    this.verifyEventsOriginalHasChanged();
                    this.buildEventFormatAndIndexed();
                }
                resolve(true);
            });
        });
    }

    public getEvent(): EventConcert {
        return this.events;
    }

    public getEventPlace(id: string): EventPlace {
        return this.events.places[id];
    }

    public getEventType(id: string): EventType {
        return this.events.types[id];
    }

    public getEventTag(id: string): EventTag {
        return this.events.tags[id];
    }

    public getFormated(): Promise<any> {
        return new Promise((resolve) => {
            // tslint:disable-next-line:max-line-length
            if (
                localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED) &&
                localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED).length > 0
            ) {
                // console.log('format events cached');
                resolve(JSON.parse(localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED)));
            } else {
                // console.log('format events not cached');
                const eventsFormated = this.generateFormatEvents();
                this.saveFormatEvents(eventsFormated);

                resolve(eventsFormated);
            }
        });
    }

    public getMyAgenda(): Promise<any> {
        // console.log('getMyAgenda');
        return new Promise(async (resolve) => {
            const events = await this.getFormated();
            const eventsSaved = await this.removeEventsNoSaved(events);
            resolve(eventsSaved);
        });
    }

    public removeEventsNoSaved(eventsFormated): Promise<any> {
        return new Promise((resolve) => {
            for (const date in eventsFormated) {
                if (eventsFormated.hasOwnProperty(date)) {
                    for (const hour in eventsFormated[date]) {
                        if (eventsFormated[date].hasOwnProperty(hour)) {
                            eventsFormated[date][hour] = eventsFormated[date][hour].filter(
                                (event) => event.save === true
                            );
                            if (eventsFormated[date][hour].length < 1) {
                                delete eventsFormated[date][hour];
                            }
                        }
                    }
                }
            }
            resolve(eventsFormated);
        });
    }

    public saveFormatEvents(eventsFormated) {
        // console.log('save format events');
        if (eventsFormated) {
            localStorage.setItem(Constants.KEY_CACHE_EVENTS_FORMATED, JSON.stringify(eventsFormated));
            this.indexFormatEvents(eventsFormated);
            return eventsFormated;
        }
    }

    public clearFormatEvents() {
        // console.log('clear format events');
        // tslint:disable-next-line:max-line-length
        if (
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED) &&
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED).length > 0
        ) {
            localStorage.removeItem(Constants.KEY_CACHE_EVENTS_FORMATED);
        }
        // tslint:disable-next-line:max-line-length
        if (
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_ORIGINAL) &&
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_ORIGINAL).length > 0
        ) {
            localStorage.removeItem(Constants.KEY_CACHE_EVENTS_ORIGINAL);
        }
    }

    public indexFormatEvents(eventsFormated) {
        // console.log('indexFormatEvents');
        this.indexLunr = lunr((builder) => {
            builder.ref('id');
            builder.field('name');
            builder.field('types');
            builder.field('tags');
            builder.field('place_name');
            builder.field('place_neighborhood');
            builder.field('place_fullname');
            builder.metadataWhitelist = ['name'];

            for (const date in eventsFormated) {
                if (eventsFormated.hasOwnProperty(date)) {
                    for (const hour in eventsFormated[date]) {
                        if (eventsFormated[date].hasOwnProperty(hour)) {
                            for (const event of eventsFormated[date][hour]) {
                                builder.add(this.eventMapperSearch(event));
                                this.listSearch[event.id] = event;
                            }
                        }
                    }
                }
            }
        });
    }

    public indexListFilter() {
        this.listFilter.places = [];
        if (!isEmpty(this.events.places)) {
            for (const key in this.events.places) {
                if (this.events.places.hasOwnProperty(key)) {
                    const place = this.events.places[key];
                    this.listFilter.places.push({
                        id: key,
                        selected: false,
                        label: `${place.name}, ${place.neighborhood}`,
                    });
                }
            }
        }

        if (!isEmpty(this.events.types)) {
            for (const key in this.events.types) {
                if (this.events.types.hasOwnProperty(key)) {
                    const type = this.events.types[key];
                    this.listFilter.types.push({
                        id: key,
                        selected: false,
                        label: type.name,
                        color: type.color,
                    });
                }
            }
        }

        if (!isEmpty(this.events.tags)) {
            for (const key in this.events.tags) {
                if (this.events.tags.hasOwnProperty(key)) {
                    const tag = this.events.tags[key];
                    this.listFilter.tags.push({
                        id: key,
                        selected: false,
                        label: tag.name,
                    });
                }
            }
        }
    }

    public eventMapperSearch(event) {
        const place = this.getEventPlace(event.place.id);
        return {
            id: event.id,
            name: event.name,
            types: event.types
                .map((type) => {
                    return this.getEventType(type.id).name;
                })
                .join(', '),
            tags: event.tags
                .map((tag) => {
                    return this.getEventTag(tag.id).name;
                })
                .join(', '),
            place_name: place.name,
            place_neighborhood: place.neighborhood,
            place_fullname: `${place.name}, ${place.neighborhood}`,
        };
    }

    public getIndexFormatEvents() {
        // console.log('getIndexFormatEvents');
        return this.indexLunr || null;
    }

    public getListSearch() {
        // console.log('getListSearch');
        return this.listSearch || null;
    }

    public getListFilter() {
        // console.log('getListFilter');
        return this.listFilter || null;
    }

    public getEventById(id) {
        // console.log('getEventById', id, this.listSearch);
        if (id) {
            return this.listSearch[id];
        }

        return null;
    }

    public getResultSearch(result: any) {
        const results = [];
        if (result) {
            const size = result.length;
            for (let i = 0; i < size; i++) {
                results.push(this.listSearch[result[i]]);
            }
            results.sort(this.sortFunction);
            return results;
        }
        return [];
    }

    public sortFunction(a, b) {
        const dateA = new Date(a.started_at).getTime();
        const dateB = new Date(b.started_at).getTime();
        return dateA > dateB ? 1 : -1;
    }

    public verifyEventsOriginalHasChanged() {
        // console.log('verify events original');

        if (
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_ORIGINAL) &&
            localStorage.getItem(Constants.KEY_CACHE_EVENTS_ORIGINAL).length > 0
        ) {
            // console.log('- exist cache');
            if (this.changeDataEvents()) {
                // console.log('-- is changed');
                this.updateFormatEvents();
                localStorage.setItem(Constants.KEY_CACHE_EVENTS_ORIGINAL, this.events.checksum);
            }
        } else {
            localStorage.setItem(Constants.KEY_CACHE_EVENTS_ORIGINAL, this.events.checksum);
        }
    }

    public updateFormatEvents() {
        // console.log('update format events');
        this.syncFormatEvents();
    }

    public syncFormatEvents() {
        // console.log('sync format events');

        const currentFormatEvents = this.generateFormatEvents();
        const pastFormatEvents = JSON.parse(localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED));

        if (pastFormatEvents) {
            for (const date in currentFormatEvents) {
                // tslint:disable-next-line:forin
                for (const hour in currentFormatEvents[date]) {
                    for (const currentEvent of currentFormatEvents[date][hour]) {
                        if (pastFormatEvents[date][hour]) {
                            for (const pastEvent of pastFormatEvents[date][hour]) {
                                if (currentEvent.id === pastEvent.id) {
                                    currentEvent.save = pastEvent.save;
                                }
                            }
                        }
                    }
                }
            }
            this.saveFormatEvents(currentFormatEvents);
        }
    }

    public changeDataEvents() {
        return localStorage.getItem(Constants.KEY_CACHE_EVENTS_ORIGINAL) !== this.events.checksum;
    }

    public generateFormatEvents() {
        const datesSize = this.events.dates.length;

        for (let i = 0; i < datesSize; i++) {
            const event = this.events.dates[i];
            const date = format(parseISO(this.events.dates[i].date), 'yyyy-MM-dd');

            if (!this.eventDays.hasOwnProperty(date)) {
                this.eventDays[date] = {};
            }

            const eventsSize = event.events.length;

            for (let j = 0; j < eventsSize; j++) {
                const eventItem = this.events.dates[i].events[j];

                const time = format(parseISO(eventItem.started_at), 'HH:mm');

                if (this.eventDays[event.date] === undefined) {
                    this.eventDays[event.date] = {};
                }

                if (this.eventDays[event.date] && !this.eventDays[event.date].hasOwnProperty(time)) {
                    this.eventDays[event.date][time] = [];
                }

                this.eventDays[event.date][time].push(eventItem);
            }
        }
        return this.eventDays;
    }

    public updateSave(eventItem): Promise<boolean> {
        // console.log('updateSave', eventItem);

        return new Promise<boolean>((resolve) => {
            // tslint:disable-next-line:max-line-length
            if (
                localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED) &&
                localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED).length > 0
            ) {
                const events = JSON.parse(localStorage.getItem(Constants.KEY_CACHE_EVENTS_FORMATED));
                // tslint:disable-next-line:forin
                dance: for (const date in events) {
                    // tslint:disable-next-line:forin
                    for (const hour in events[date]) {
                        for (const event of events[date][hour]) {
                            if (event.id === eventItem.id) {
                                event.save = eventItem.save;
                                this.listSearch[event.id] = event;
                                break dance;
                            }
                        }
                    }
                }
                localStorage.setItem(Constants.KEY_CACHE_EVENTS_FORMATED, JSON.stringify(events));
                resolve(true);
            }
        });
    }

    public getEventDates() {
        return this.eventDates;
    }

    filterEventsFormated(filters): Promise<EventAttractionDay> {
        // console.log('filterEventsFormated', filters);

        return this.getFormated().then((eventFiltered) => {
            const copyEventFiltered = cloneDeep(eventFiltered);
            for (const date in eventFiltered) {
                // tslint:disable-next-line:forin
                for (const hour in eventFiltered[date]) {
                    eventFiltered[date][hour].forEach((event, index) => {
                        const existTypes = event.types.some((type) =>
                            filters.types.some((typeFilter) => type.id === typeFilter.id)
                        );
                        const existTags = event.tags.some((tag) =>
                            filters.tags.some((tagFilter) => tag.id === tagFilter.id)
                        );
                        const existPlaces = filters.places.some((place) => event.place.id === place.id);
                        if (!existTypes && !existTags && !existPlaces) {
                            copyEventFiltered[date][hour][index] = {};
                        }
                    });
                }
            }
            // console.log('filterEventsFormated copyEventFiltered', copyEventFiltered);
            return this.removeDateEventsEmpty(copyEventFiltered);
        });
    }

    public removeDateEventsEmpty(events: EventAttractionDay): EventAttractionDay {
        if (events) {
            for (const date in events) {
                // tslint:disable-next-line:forin
                for (const hour in events[date]) {
                    // tslint:disable-next-line:max-line-length
                    if (events[date][hour] && events[date][hour].length) {
                        let canDeleteHour = true;
                        events[date][hour].forEach((show) => {
                            if (!isEmpty(show)) {
                                canDeleteHour = false;
                                return true;
                            }
                        });

                        if (canDeleteHour) {
                            delete events[date][hour];
                        }
                    }
                }
            }
            return events;
        }
        return {};
    }

    async hasEventSaved(): Promise<boolean> {
        const events = await this.getFormated();
        return new Promise<boolean>((resolve, reject) => {
            if (events) {
                for (const date in events) {
                    // tslint:disable-next-line:forin
                    for (const hour in events[date]) {
                        // tslint:disable-next-line:max-line-length
                        for (const event of events[date][hour]) {
                            if (event.save) {
                                resolve(true);
                            }
                        }
                    }
                }
            }
            resolve(false);
        });
    }

    public saveEvents(eventsImport: Array<string>) {
        // console.log('saveEvents', eventsImport);

        eventsImport.forEach(async (eventId) => {
            const event = {
                id: eventId,
                save: true,
            };

            await this.updateSave(event);
        });
    }
}
