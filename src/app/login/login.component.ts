import { GoogleAnalyticsService } from './../providers/google-analytics.service';
import { Constants } from './../utils/constants';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
    constructor(private router: Router, private ga: GoogleAnalyticsService) {}

    ngOnInit() {
        // this.carouselOne = {
        //   grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
        //   slide: 1,
        //   speed: 400,
        //   interval: 10000,
        //   point: {
        //     visible: true
        //   },
        //   load: 2,
        //   touch: true,
        //   loop: false,
        //   custom: 'banner'
        // };
    }

    login() {
        this.ga.sendEvent('login', 'click', 'enter');

        localStorage.setItem(Constants.KEY_EVENT_NAME, 'true');
        this.router.navigate(['home/my-agenda']);
    }
}
