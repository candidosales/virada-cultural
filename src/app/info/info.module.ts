import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from './../shared/shared.module';
import { InfoRoutingModule } from './info-routing.module';
import { InfoComponent } from './info.component';

@NgModule({
    imports: [SharedModule, InfoRoutingModule, FormsModule, TranslateModule.forChild()],
    declarations: [InfoComponent],
    providers: [],
})
export class InfoModule {}
