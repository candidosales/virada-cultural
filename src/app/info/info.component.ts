import { EventService } from './../providers/event.service';
import { EventConcert } from './../interface/event_concert.interface';
import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from '../providers/google-analytics.service';
import { ThemeService } from '../theme/theme.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
})
export class InfoComponent implements OnInit {
    public event: EventConcert;
    public panelOpenState = false;
    public telephones = [
        {
            title: 'Delegacia da Mulher',
            description: 'Atendimento',
            numbers: ['180'],
        },
        {
            title: 'Polícia',
            description: 'Atendimento',
            numbers: ['190'],
        },
        {
            title: 'Ambulância',
            description: 'Atendimento',
            numbers: ['192'],
        },
        {
            title: 'Bombeiros',
            description: 'Atendimento',
            numbers: ['193'],
        },
        {
            title: 'Bombeiros e Urgência',
            description: 'Atendimento',
            numbers: ['11 3396-2000'],
        },
        {
            title: 'SAMU',
            description: 'Atendimento',
            numbers: ['192', '11 3396-1400'],
        },
        {
            title: 'Secretaria de Cultura',
            description: 'Atendimento',
            numbers: ['0800 011 0156'],
        },
    ];

    public configs = {};

    public selectedOptionsConfigs = [];

    constructor(
        private eventService: EventService,
        private ga: GoogleAnalyticsService,
        private themeService: ThemeService,
        private translate: TranslateService
    ) {
        this.configs = {
            darkTheme: {
                icon: 'brightness_4',
                name: this.translate.instant('info.config.itemDarkTheme.title'),
                description: this.translate.instant('info.config.itemDarkTheme.description'),
                selected: this.themeService.getCacheTheme() === 'dark' ? true : false,
            },
        };
    }

    ngOnInit() {
        this.event = this.eventService.getEvent();
    }

    sendEvent(label: string) {
        this.ga.sendEvent('info', 'click', label);
    }

    onConfigChange(config: any) {
        config.selected = !config.selected;
        this.toggleTheme();
    }

    toggleTheme() {
        const active = this.themeService.getActiveTheme();
        this.themeService.setTheme(active.name === 'light' ? 'dark' : 'light');
    }
}
