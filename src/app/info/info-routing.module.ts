import { InfoComponent } from './info.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const InfoRoutes: Routes = [
    {
        path: '',
        component: InfoComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(InfoRoutes)],
    exports: [RouterModule],
})
export class InfoRoutingModule {}
