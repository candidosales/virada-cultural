import { GoogleAnalyticsService } from './../providers/google-analytics.service';

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import { EventConcert } from './../interface/event_concert.interface';
import { EventService } from './../providers/event.service';
import { Observable, Subject, of } from 'rxjs';

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
})
export class SearchComponent implements OnInit {
    @ViewChild('searchBox', { static: true }) searchBox: ElementRef;
    public events$: Observable<any[]>;
    private searchTerms = new Subject<string>();
    private indexes;
    public listFilters: any;
    public hasResult = true;
    public showLoading = false;
    public searchTerm = '';

    constructor(private eventService: EventService, private ga: GoogleAnalyticsService, private router: Router) {
        this.indexes = this.eventService.getIndexFormatEvents();
    }

    ngOnInit(): void {
        // console.log('init Search');

        this.listFilters = this.eventService.getListFilter();

        this.events$ = this.searchTerms.pipe(
            // wait 300ms after each keystroke before considering the term
            debounceTime(300),

            // ignore new term if same as previous term
            distinctUntilChanged(),

            // switch to new search observable each time the term changes
            switchMap((term: string) => {
                this.ga.sendEvent('search', 'type', term);
                const result = this.searchIndex(term);

                this.hasResult = result.length > 0 ? true : false;
                this.showLoading = false;

                return of(result);
            })
        );
        this.focusInputSearch();
    }

    focusInputSearch() {
        this.searchBox.nativeElement.focus();
    }

    clickSearch(term: string) {
        this.ga.sendEvent('search', 'click', term);

        const termClear = this.clearTerm(term);
        this.searchBox.nativeElement.value = termClear;
        this.search(termClear);

        this.forceKeyUp();
    }

    clearTerm(term: string): string {
        // return term.normalize('NFD').replace(/[\u0300-\u036f]/g, '').replace(/[^a-zA-Z0-9 ]/g, '');
        return term.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
    }

    forceKeyUp() {
        setTimeout(() => {
            const elementSearch: HTMLElement = document.getElementById('search-box');
            elementSearch.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter' }));
        }, 500);
    }

    searchIndex(term: string): Array<EventConcert> {
        const resultRef = this.indexes.search(term + '*');
        return this.eventService.getResultSearch(resultRef.map((item) => item.ref));
    }

    search(term: string): void {
        if (term.length > 0) {
            this.showLoading = true;
            this.searchTerms.next(term);
        }
    }

    get hasTags(): boolean {
        return this.listFilters.tags.length > 0 || false;
    }
}
