import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from './../shared/shared.module';
import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';

@NgModule({
    imports: [SharedModule, SearchRoutingModule, TranslateModule.forChild()],
    declarations: [SearchComponent],
    providers: [],
})
export class SearchModule {}
