import { SearchComponent } from './search.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const SearchRoutes: Routes = [
    {
        path: '',
        component: SearchComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(SearchRoutes)],
    exports: [RouterModule],
})
export class SearchRoutingModule {}
