# App My Agenda

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Service Worker
https://medium.com/google-developer-experts/a-new-angular-service-worker-creating-automatic-progressive-web-apps-part-2-practice-3221471269a1

https://angularfirebase.com/lessons/hnpwa-angular-5-progressive-web-app-service-worker-tutorial/


## Deploy
https://scotch.io/tutorials/deploying-an-angular-cli-app-to-production-with-firebase


## Sobrescrever o não cache do servicer worker pelo firebase
https://github.com/facebook/create-react-app/issues/2440#issuecomment-305592301

## Performance
https://slides.com/webmax/angular-perf-amsterdamjs
https://medium.freecodecamp.org/how-to-get-the-best-performance-out-of-your-angular-apps-d5132a6c3335

# Checker Favicon
https://realfavicongenerator.net
https://app-manifest.firebaseapp.com/
https://www.favicon-generator.org/

# Custom iOS SplashScreen
https://appsco.pe/developer/splash-screens


# References

- SSR: https://slides.com/williamgrasel/e-o-angular-ainda-vive#/3/5
- Melhorar experiência PWA para iOS (https://medium.com/@oieduardorabelo/pwa-no-ios-como-deix%C3%A1-lo-mais-nativo-e022f6c67b9c); 
- Como usar o CDKScrollable do Material Angular - fail;
- Trocar momento por date-fns: https://github.com/date-fns/date-fns (https://medium.com/@addyosmani/the-cost-of-javascript-in-2018-7d8950fbb5d4) - ok
- font-display: swap;
- <link rel="preload">
- IndexedDb - ok
- https://www.npmtrends.com/dexie-vs-localforage-vs-lokijs-vs-lowdb-vs-nedb-vs-nedb-promise-vs-node-json-db-vs-pouchdb-vs-tingodb-vs-warehouse
- bundlephobia;
- Theming - https://medium.com/@amcdnl/theming-angular-with-css-variables-3c78a5b20b24 - ok
- Ngx Build Modern: https://twitter.com/mgechev/status/1081235720616914944?s=19 - ok
- https://twitter.com/mgechev/status/1078193518445641728?s=19 
- Angular Preload data: https://devblog.dymel.pl/2017/10/17/angular-preload/ - ok
- IntersectionObserver - https://blog.angularindepth.com/a-modern-solution-to-lazy-loading-using-intersection-observer-9280c149bbc / https://github.com/TradeMe/ng-defer-load#readme
- Socket: https://github.com/googollee/go-socket.io
- https://hashrocket.com/blog/posts/websocket-shootout
- https://dev.to/uyouthe/pwa-checklist-2019-25j4
- Angular Maps: https://blog.angularindepth.com/google-maps-is-now-an-angular-component-821ec61d2a0
- PWA install: https://medium.com/pwabuilder/installing-progressive-web-apps-b7274ebffe80
- SEO: https://www.npmjs.com/package/ngx-seov

// https://developer.chrome.com/devsummit/schedule/
// --blue: hsl(214 40% 55%);
// --lightblue: hsl(213 57% 10%);
// --darkblue: hsl(214 70% 59%);
// --red: hsl(4 50% 40%);
// --white: hsl(200 10% 10%);
// --offwhite: hsl(200 10% 13%);
// --lessdarkoffwhite: hsl(200 10% 16%);
// --lesslessdarkoffwhite: hsl(200 10% 20%);
// --darkoffwhite: hsl(200 10% 25%);
// --lightlightgrey: hsl(200 10% 40%);
// --lightgrey: hsl(200 10% 50%);
// --midgrey: hsl(200 10% 60%);
// --grey: hsl(200 10% 70%);
// --darkgrey: hsl(200 10% 75%);
// --offblack: hsl(200 10% 80%);
// --black: hsl(200 10% 90%);
// --black-a50: hsla(0 0% 0%/90%);


# Melhorias de Usabilidade

## 4v

- Purge css: https://medium.com/@joao.a.edmundo/angular-cli-tailwindcss-purgecss-2853ef422c02

## 3v

- Qtd de blocos por categoria, Tag e Endereço
- No mapa opção para - Proximo shows perto de mim;
- Alerta por eventos com o mesmo horário já adicionado;
- i18n - https://github.com/ngx-translate/core - ok;
- Replace @agm/core to @angular/google-maps - ok;
- Replace @ngu/carousel to @ng-bootstrap/carousel; - ok
- Angular and Material 10 - ok;
- Melhoria nas cores do dark theme - ok;
- Espanhol - ok;
 
## 2v

- Angular 8;
- Substituicão do moment por date-fn;
- Separacão do conteúdo dos eventos do código fonte;
- Modularizar e split de funcionalidades;
- Checkbox - Pensou que do item da frente - Usabilidade;
- Filtros - não tem opção para fechar a caixa - Usabilidade;
- Internacionalização - i18n;
- Colocar o dia da semana nas tabs (seg, ter, qua) - i18n;
- Versão desktop;
- Migrar para bootstrap grid;
- Modo noturno;
- Melhorar apresentacao dos filtros : Google io 2018
- Melhorar apresentacao das horas: Google IO 2018 (position: sticky, nos horários e datas);

## 1v
