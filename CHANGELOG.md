# Changelog

## [3.8.0] - 2022-11-25

### Added

-   Upgrade to Angular 14.2.12;
-   Upgrade to Angular Material 14.2.7;
-   Upgrade libs;

## [3.7.0] - 2022-01-27

### Added

-   Upgrade to Angular 13.2.0;
-   Upgrade to Angular Material 13.2.0;
-   Upgrade libs;

## [3.6.0] - 2021-11-04

### Added

-   Upgrade to Angular 13.0.0;
-   Upgrade to Angular Material 13.0.0;
-   Upgrade libs;

## [3.5.0] - 2021-05-12

### Added

-   Upgrade to Angular 12.0.0;
-   Upgrade to Angular Material 12.0.0;

## [3.4.0] - 2020-11-11

### Added

-   Prettier;
-   Upgrade to Angular 11.0.0;
-   Upgrade to Angular Material 11.0.0;
-   Using Hot Module Replacement using Webpack 5 `npm run start`;

### Changed

-   Improve the UI at the Search Page;
-   Improve the load web fonts;

### Fixed

-   Replace `async/await` by BehaviorSubject because Zone.js doesn't support it. See: https://github.com/angular/zone.js/pull/1140 for more information.

## [3.3.0] - 2020-10-03

### Added

-   Prettier;
-   Upgrade to Angular 10.1.4;
-   Upgrade to Angular Material 10.2.3;
-   Upgrade Typescript 4.0;

## [3.2.0] - 2020-06-28

### Added

-   Upgrade to Angular 10;
-   Upgrade to Angular Material 10;
-   Upgrade Typescript 3.9;

### Changed

-   Replace `lodash` to `lodash-es`;
-   Replace `lodash-decorator` to `memo-decorator`;
-   Remove hammerjs: https://github.com/angular/components/blob/3a204da37fd1366cae411b5c234517ecad199737/guides/v9-hammerjs-migration.md#how-to-migrate-my-tests

## [3.1.1] - 2020-02-08

### Added

-   Update useful numbers;
-   Added spanish translation;

### Fixed

-   Translation in `event.component.ts` and `event-item.component.ts`;

## [3.1.0] - 2020-02-08

### Added

-   Upgrade Angular, Material to 9 release final;
-   Upgrade Typescript 3.7;

## [3.0.0] - 2019-11-24

### Added

-   Upgrade to Angular Material 9;
-   Improvements in Login page to desktop;
-   Improve colors to dark theme based on the ChromeDevSummit website;
-   Improve load web fonts (Google Sans and Material Icons);
-   Preconnet to web fonts: https://developers.google.com/web/fundamentals/performance/resource-prioritization?utm_source=lighthouse&utm_medium=devtools#preconnect;
-   Add lazy loading to images: https://web.dev/native-lazy-loading/;
-   Add prompt to install PWA to iPhone/iPad/iPod (https://developers.google.com/web/fundamentals/app-install-banners/promoting-install-mobile);
-   Translated to english (https://github.com/ngx-translate/core / https://indepth.dev/implementing-multi-language-angular-applications-rendered-on-server/);
-   Performance improvemente in 19.42x faster `generateFormatEvents`, just replacing `forEach` to `for`

### Changed

-   Replace `@agm/core` to `@angular/google-maps`;
-   Replace `@ngu/carousel` to `@ng-bootstrap`;
-   Remove `for in` to improve performance: https://www.youtube.com/watch?v=x7Xzvm0iLCI'

### Fixed

-   Upgrade `date-fns` and fix break changes;

## [2.2.1] - 2019-08-19

### Added

-   Upgrade to Angular 9;
-   Upgrade libs;

## [2.2.0] - 2019-07-09

### Added

-   Upgrade to Angular 8;
-   Upgrade libs;

### Changed

-   Adapt lazy loading modules to load via import;

### Removed

-   ngx-build-plus and ngx-build-modern;
