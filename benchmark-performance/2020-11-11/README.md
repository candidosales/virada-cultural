## Angular 11.0.0 Final

```shell
Initial Chunk Files                      | Names         |      Size
main-es5.2b8c1fad508f4b5150ee.js         | main          |   1.01 MB
main-es2018.2b8c1fad508f4b5150ee.js      | main          | 911.26 kB
styles.2484d99d4c702e9bb79b.css          | styles        | 139.46 kB
polyfills-es5.1e65c6a992c551a244cd.js    | polyfills-es5 | 129.32 kB
polyfills-es2018.52df7e0a9c9941d29a03.js | polyfills     |  36.51 kB
runtime-es2018.a0618ffa112f31f7d041.js   | runtime       |   2.36 kB
runtime-es5.a0618ffa112f31f7d041.js      | runtime       |   2.36 kB

                                         | Initial Total |   2.20 MB

Lazy Chunk Files                         | Names         |      Size
6-es5.abc46e98f211c0e313db.js            | -             |  25.73 kB
6-es2018.abc46e98f211c0e313db.js         | -             |  21.28 kB
9-es5.0c727217bc13b4db3949.js            | -             |  17.58 kB
7-es5.f81a0762f5bb11335ada.js            | -             |  14.92 kB
9-es2018.0c727217bc13b4db3949.js         | -             |  14.70 kB
7-es2018.f81a0762f5bb11335ada.js         | -             |  13.80 kB
8-es5.09bdb70b7022e9c9fb1c.js            | -             |   9.41 kB
8-es2018.09bdb70b7022e9c9fb1c.js         | -             |   9.00 kB
10-es5.590e2076ac07c2eb23ea.js           | -             |   6.04 kB
10-es2018.590e2076ac07c2eb23ea.js        | -             |   5.55 kB
common-es5.8167f01dbfe913cfb634.js       | common        |   4.29 kB
common-es2018.8167f01dbfe913cfb634.js    | common        |   3.07 kB

Build at: 2020-11-12T04:17:13.906Z - Hash: 25d03e5c662766649df3 - Time: 71323ms
```
