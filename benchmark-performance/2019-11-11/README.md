# Benchmarking

## Angular 8.1.0

```bash
chunk {0} common-es2015.3d3c4b484b4bdfbfe129.js (common) 9.28 kB  [rendered]
chunk {1} runtime-es2015.f8b207c9d0d2835345cc.js (runtime) 2.34 kB [entry] [rendered]
chunk {2} 2-es2015.d31202a84e9c377b0552.js () 15.6 kB  [rendered]
chunk {3} main-es2015.fd35e8bb16386bc17ebc.js (main) 830 kB [initial] [rendered]
chunk {4} polyfills-es2015.da7489c5970814d972fd.js (polyfills) 36.4 kB [initial] [rendered]
chunk {5} styles.813310209d2dcfd2efc3.css (styles) 139 kB [initial] [rendered]
chunk {6} 6-es2015.b751b327ee7acc2449ef.js () 43.9 kB  [rendered]
chunk {7} 7-es2015.3590d98b3bad4c284246.js () 24.5 kB  [rendered]
chunk {8} 8-es2015.0c251188c790d7e8d638.js () 27.7 kB  [rendered]
chunk {9} 9-es2015.51e05f268851ed1832b5.js () 27.2 kB  [rendered]
chunk {10} 10-es2015.50ebf476f51b2b07fc42.js () 32.7 kB  [rendered]
Date: 2019-11-11T16:54:27.496Z - Hash: 45b7463df0a6ffc5c957 - Time: 30236ms

chunk {0} common-es5.14b7efaf87a723b08c1b.js (common) 10.6 kB  [rendered]
chunk {1} runtime-es5.eba4b55fd0388f34479a.js (runtime) 2.34 kB [entry] [rendered]
chunk {2} 2-es5.e41139a98db7664c49f6.js () 15.6 kB  [rendered]
chunk {3} main-es5.0b5f13e777cc37b66f90.js (main) 930 kB [initial] [rendered]
chunk {4} polyfills-es5.b5979e34fb8b78ae6e7e.js (polyfills) 113 kB [initial] [rendered]
chunk {5} 5-es5.46656dce2f40373f418b.js () 44.5 kB  [rendered]
chunk {6} 6-es5.ad085cce8dd92908dae8.js () 24.6 kB  [rendered]
chunk {7} 7-es5.8d5650a7fd6650c0491a.js () 30.7 kB  [rendered]
chunk {8} 8-es5.54b4af49ce8efc3bba33.js () 29.3 kB  [rendered]
chunk {9} 9-es5.e7c6566564477af1c779.js () 33 kB  [rendered]
Date: 2019-11-11T16:54:52.337Z - Hash: 1e61ecada14c1fe3779b - Time: 24742ms
```

## Angular 9.0.0

```bash
chunk {3} polyfills-es5.d179b1907acb88ba1afc.js (polyfills-es5) 126 kB [initial] [rendered]
chunk {2} polyfills-es2015.5e3916d3146c907cb3a5.js (polyfills) 36.6 kB [initial] [rendered]
chunk {0} runtime-es2015.cecd6166f1a6ea9bcb98.js (runtime) 2.33 kB [entry] [rendered]
chunk {0} runtime-es5.cecd6166f1a6ea9bcb98.js (runtime) 2.33 kB [entry] [rendered]
chunk {6} 6-es2015.a84fc656d0e4cb8f291f.js () 8.92 kB  [rendered]
chunk {6} 6-es5.a84fc656d0e4cb8f291f.js () 9.46 kB  [rendered]
chunk {5} 5-es2015.f6b333360fdfd21a04c0.js () 19.2 kB  [rendered]
chunk {5} 5-es5.f6b333360fdfd21a04c0.js () 23.3 kB  [rendered]
chunk {7} 7-es2015.9b97501fb68311b5d649.js () 11.7 kB  [rendered]
chunk {7} 7-es5.9b97501fb68311b5d649.js () 13.3 kB  [rendered]
chunk {9} 9-es2015.d11ec87ea4676245dab5.js () 5.3 kB  [rendered]
chunk {9} 9-es5.d11ec87ea4676245dab5.js () 5.92 kB  [rendered]
chunk {8} 8-es2015.3da21273edbea9232a35.js () 13.3 kB  [rendered]
chunk {8} 8-es5.3da21273edbea9232a35.js () 15 kB  [rendered]
chunk {1} main-es2015.a6ba61ca5f51e88dc1e9.js (main) 924 kB [initial] [rendered]
chunk {1} main-es5.a6ba61ca5f51e88dc1e9.js (main) 1.04 MB [initial] [rendered]
chunk {4} styles.223aaa89c50a1279b5b0.css (styles) 142 kB [initial] [rendered]
Date: 2019-11-11T17:04:44.744Z - Hash: 285cbee9d113249f1344 - Time: 106130ms
```
