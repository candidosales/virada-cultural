# JavaScript Loops - Code This, Not That

https://www.youtube.com/watch?v=x7Xzvm0iLCI


## generateFormatEvents

### forEach

```bash
generateFormatEvents: 98.423095703125ms
generateFormatEvents: 85.47607421875ms
generateFormatEvents: 70.083740234375ms
generateFormatEvents: 93.48876953125ms
```

avg: 86.84ms

### for

```bash
generateFormatEvents for: 4.56787109375ms
generateFormatEvents for: 4.5361328125ms
generateFormatEvents for: 4.34619140625ms
generateFormatEvents for: 4.48583984375ms
```

avg: 4.47ms

https://stackoverflow.com/questions/28403939/how-to-calculate-percentage-improvement-in-response-time-for-performance-testing/28404036

## Your code's runtime is XX.XX% shorter/improved/decreased:

(new - old) / old x 100%
(4.47 - 86.84) / 86.84 * 100% =~ -94.85% (minus represents decrease)

## Your code is XXXX% faster

(old - new) / new x 100%
(86.84 - 4.47) / 4.47 * 100% =~ 1842% (faster)

## Your new time is Xx faster

old/new = 86.84/4.47 = 19.42x


## buildEventDates

### forEach

```
buildEventDates forEach: 0.045166015625ms
buildEventDates forEach: 0.046875ms
buildEventDates forEach: 0.05517578125ms
buildEventDates forEach: 0.0478515625ms
```

### for

```
buildEventDates for: 0.032958984375ms
buildEventDates for: 0.035888671875ms
buildEventDates for: 0.164794921875ms
buildEventDates for: 0.0361328125ms
buildEventDates for: 0.036865234375ms
```

## getResultSearch

```
event.service.ts:268 getResultSearch forEach: 0.00732421875ms
event.service.ts:268 getResultSearch forEach: 0.031005859375ms
event.service.ts:268 getResultSearch forEach: 0.02685546875ms
event.service.ts:268 getResultSearch forEach: 0.028076171875ms
event.service.ts:268 getResultSearch forEach: 0.02099609375ms
```

```
getResultSearch for: 0.032958984375ms
event.service.ts:273 getResultSearch for: 0.031005859375ms
event.service.ts:273 getResultSearch for: 0.05810546875ms
event.service.ts:273 getResultSearch for: 0.02294921875ms
event.service.ts:273 getResultSearch for: 0.054931640625ms
```
