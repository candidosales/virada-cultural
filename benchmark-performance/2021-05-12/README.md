## Angular 12.0.0 Final

```shell
Initial Chunk Files                      | Names                |       Size
main-es5.298713aa449d37040629.js         | main                 |  950.63 kB
main-es2018.298713aa449d37040629.js      | main                 |  837.38 kB
styles.24cfc5fd1dc9d4876ea7.css          | styles               |  145.34 kB
polyfills-es5.afd77528dafb71b01ba1.js    | polyfills-es5        |  129.74 kB
polyfills-es2018.338a61ff2eb3e43f30a8.js | polyfills            |   36.73 kB
runtime-es2018.6894cb4ceef0178024fd.js   | runtime              |    3.41 kB
runtime-es5.6894cb4ceef0178024fd.js      | runtime              |    3.41 kB

                                         | Initial ES5 Total    |    1.20 MB
                                         | Initial ES2018 Total | 1022.86 kB

Lazy Chunk Files                         | Names                |       Size
686-es5.d837820fe248cbbe12fe.js          | -                    |   23.96 kB
686-es2018.d837820fe248cbbe12fe.js       | -                    |   19.81 kB
451-es5.ab4bf50cb8dab83d48da.js          | -                    |   17.69 kB
75-es5.5fb44cf56563e40e3ddd.js           | -                    |   15.36 kB
451-es2018.ab4bf50cb8dab83d48da.js       | -                    |   15.13 kB
75-es2018.5fb44cf56563e40e3ddd.js        | -                    |   14.17 kB
102-es5.8b1ad5463bb4991b9cee.js          | -                    |    9.75 kB
102-es2018.8b1ad5463bb4991b9cee.js       | -                    |    9.31 kB
711-es5.cb4f0cd259162a2c9098.js          | -                    |    6.20 kB
711-es2018.cb4f0cd259162a2c9098.js       | -                    |    5.68 kB
592-es5.7c0c370117b590a3aea5.js          | common               |    4.02 kB
592-es2018.7c0c370117b590a3aea5.js       | common               |    3.21 kB
```
